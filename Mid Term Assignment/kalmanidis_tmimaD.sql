-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for a20etairia
CREATE DATABASE IF NOT EXISTS `a20etairia` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `a20etairia`;

-- Dumping structure for table a20etairia.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `depid` int(11) NOT NULL,
  `depname` varchar(60) NOT NULL,
  `manager` int(11) NOT NULL,
  `location` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`depid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table a20etairia.departments: ~5 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`depid`, `depname`, `manager`, `location`) VALUES
	(1, 'MANAGEMENT', 109, 'SINTAGMA'),
	(2, 'ACCOUNTING', 153, 'PERISTERI'),
	(3, 'ENGINEER', 431, 'PERISTERI'),
	(4, 'MARKETING', 189, 'EGALEO'),
	(5, 'LOGISTICS', 102, 'EGALEO');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table a20etairia.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `empid` int(11) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `lastname` varchar(40) NOT NULL,
  `ptixio` varchar(15) DEFAULT NULL,
  `hiredate` date DEFAULT NULL,
  `depid` int(11) NOT NULL,
  `salary` decimal(6,2) DEFAULT NULL,
  `bonus` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`empid`),
  KEY `depid` (`depid`),
  CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`depid`) REFERENCES `departments` (`depid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table a20etairia.employees: ~24 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`empid`, `firstname`, `lastname`, `ptixio`, `hiredate`, `depid`, `salary`, `bonus`) VALUES
	(102, 'NIKOS', 'DIAMANTIDHS', 'PhD', '2003-06-02', 5, 1212.50, 520.50),
	(109, 'MARIA', 'ATHANASIOU', 'MBA', '2000-01-26', 1, 2787.69, NULL),
	(153, 'MARIA', 'ALEBIZATOU', 'MBA', '2001-05-15', 2, 1321.92, 520.50),
	(189, 'KOSTAS', 'AGGELOU', 'PhD', '2000-06-19', 1, 1908.28, 800.50),
	(206, 'NIKOS', 'BLAHOS', 'MSc', '2002-12-03', 4, 1102.04, NULL),
	(342, 'KOSTAS', 'FRAGOULIS', 'MSc', '2001-04-17', 4, 1351.96, NULL),
	(419, 'PETROS', 'ARVANITOU', 'MBA', '2000-07-17', 2, 1323.80, 200.50),
	(431, 'KOSTAS', 'PAPADOPOULOS', 'MSc', '2002-09-16', 3, 1100.23, 200.50),
	(483, 'HRAKLIS', 'MANVLAKHS', 'MSc', '2003-09-18', 3, NULL, NULL),
	(502, 'MAIRH', 'MAROU', 'MSc', '2001-03-07', 1, 1754.67, 520.50),
	(543, 'MANOLHS', 'BOURNAS', 'MSc', '2000-05-25', 3, 1321.77, 520.50),
	(593, 'PAVLOS', 'PERIDHS', 'MSc', '2004-10-05', 4, NULL, NULL),
	(780, 'EYTHIMIA', 'MINOU', 'MSc', '2002-12-03', 4, 1054.71, NULL),
	(801, 'XRHSTOS', 'ALEXIOU', 'MBA', '1999-07-16', 3, 2312.56, 200.50),
	(805, 'STEFANIA', 'DERI', 'MSc', '2003-01-08', 3, 2100.00, 240.00),
	(811, 'NIKH', 'BASILAKH', 'MSc', '2002-07-01', 2, 1323.98, 300.60),
	(860, 'NASOS', 'NASTOULIS', 'MBA', '2003-01-09', 5, 1300.00, 200.00),
	(862, 'LAKIS', 'BLAHOS', 'MBA', '2003-01-09', 5, 2200.00, 180.00),
	(901, 'KYRIAKOS', 'PAPPAS', 'MSc', '2001-11-01', 1, 1852.99, 300.60),
	(3101, 'THODORIS', 'KALMANIDIS', 'MBA', '2001-01-31', 1, 1500.00, 100.00),
	(3102, 'MATOULA', 'MONIOU', 'PhD', '2021-01-31', 1, 1200.00, 250.00),
	(3103, 'DIMITRIS', 'KAGKAS', 'MSc', '2004-06-15', 1, 2500.00, NULL),
	(3104, 'KATERINA', 'VENERI', 'MBA', '2003-11-28', 1, 1000.00, 300.00),
	(3105, 'PAUL', 'PSOMIADIS', 'PhD', '2001-05-03', 1, 1600.00, NULL);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table a20etairia.projects
CREATE TABLE IF NOT EXISTS `projects` (
  `projid` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `budget` decimal(8,2) NOT NULL DEFAULT 0.00,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `progress` decimal(3,1) NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`projid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table a20etairia.projects: ~6 rows (approximately)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`projid`, `title`, `budget`, `startdate`, `enddate`, `progress`) VALUES
	(5, 'Epivlepsi aerodromiou', 45000.00, '2005-06-01', '2006-12-31', 25.0),
	(12, 'Stathmos epexergasias limaton', 124000.00, '2006-06-01', '2007-05-31', 60.0),
	(14, 'Meleti neou limena', 383500.00, '2006-04-01', '2010-05-31', 20.0),
	(21, 'Simvouletikes ypiresies', 50000.00, '2003-06-01', '2005-05-31', 85.0),
	(38, 'Meleti Mall', 65000.00, '2007-08-01', '2007-07-31', 0.0),
	(43, 'New bridge', 175000.00, '2003-04-15', '2005-04-15', 75.0);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

-- Dumping structure for table a20etairia.workson
CREATE TABLE IF NOT EXISTS `workson` (
  `empid` int(11) NOT NULL,
  `projid` int(11) NOT NULL,
  PRIMARY KEY (`empid`,`projid`),
  KEY `projid` (`projid`),
  CONSTRAINT `workson_ibfk_1` FOREIGN KEY (`empid`) REFERENCES `employees` (`empid`) ON UPDATE CASCADE,
  CONSTRAINT `workson_ibfk_2` FOREIGN KEY (`projid`) REFERENCES `projects` (`projid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table a20etairia.workson: ~24 rows (approximately)
/*!40000 ALTER TABLE `workson` DISABLE KEYS */;
INSERT INTO `workson` (`empid`, `projid`) VALUES
	(102, 38),
	(109, 14),
	(153, 14),
	(153, 38),
	(189, 14),
	(189, 21),
	(189, 43),
	(206, 12),
	(342, 12),
	(342, 14),
	(342, 38),
	(419, 14),
	(419, 38),
	(431, 21),
	(431, 38),
	(483, 12),
	(502, 5),
	(502, 12),
	(543, 21),
	(593, 43),
	(780, 14),
	(801, 21),
	(811, 12),
	(811, 21);
/*!40000 ALTER TABLE `workson` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
