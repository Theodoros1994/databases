
--   /************************* PART B ***********************************/


-- B2) Να βρεθεί ο κωδικός του τμήματος όπου εργάζεται ο υπάλληλος με κωδικό 109 και 431.


SELECT d.depid, e.empid FROM departments d
INNER JOIN employees e ON e.depid= d.depid
WHERE e.empid IN ('109','431');

-- B3) Να βρεθεί ο μισθός του υπαλλήλου με κωδικό 801.

SELECT salary FROM employees WHERE empid='801';


-- B4) Να βρεθεί το όνομα, επώνυμο, μισθός και το τμήμα όλων των υπαλλήλων που προσελήφθησαν μετά την ημερομηνία 1/2/2002 αλλά πρίν από το 2005.

SELECT e.firstname,e.lastname,e.salary,d.depname FROM employees e
INNER JOIN departments d ON e.depid=d.depid
WHERE e.hiredate BETWEEN '2002-02-01' AND '2005-01-01';


-- B5) Να βρεθεί ο κωδικός και το όνομα των υπαλλήλων που, αν πάρουν αύξηση 6%, ο μισθός τους θα μεγαλώσει κατά ποσό μεγαλύτερο των 75 €.

SELECT e.firstname,e.lastname,e.empid FROM employees e
WHERE ((e.salary*6)/100 )  >75;





-- B6) Να βρεθεί ο κωδικός και η ημερομηνία λήξης όλων των έργων, με φθίνουσα ταξινόμηση ως προς την ημερομηνία έναρξης.

SELECT p.projid, p.enddate FROM projects p
ORDER BY p.startdate desc;


-- B7) Να βρεθεί η ημερομηνία πρόσληψης όλων των υπαλλήλων που το επώνυμό τους αρχίζει από Α.

SELECT e.hiredate,e.lastname FROM employees e 
WHERE e.lastname LIKE 'A%';


-- B8) Να βρεθούν ο κωδικός και η ημερομηνία πρόσληψης όλων των υπαλλήλων των τμημάτων 3 και 4.
SELECT e.hiredate,e.empid FROM employees e
WHERE e.depid IN ('3','4');

-- B9)Να βρεθούν όλες οι λεπτομέρειες των υπαλλήλων που το όνομά τους αρχίζει από Μ ή Κ και τελειώνει σε s.

SELECT * FROM employees e WHERE e.firstname LIKE 'M%s' OR e.firstname LIKE 'K%s';

-- B10) Να βρεθεί ο κωδικός, το τμήμα, και το επώνυμο όλων των υπαλλήλων με μισθό στην περιοχή [1100,1600) € που εργάζονται στα τμήματα 1, 3, και 4.

SELECT e.empid, d.depname, e.lastname FROM employees e 
INNER JOIN departments d ON d.depid=e.depid
WHERE d.depid IN ('1','3','4') AND (e.salary>=1100 AND e.salary <1600);



-- Β11) Να βρεθεί ο κωδικός και το όνομα των υπαλλήλων στα τμήματα 1, 2, και 4 που ο μισθός τους είναι μεταξύ 1000 και 1500 €, με φθίνουσα ταξινόμηση ως προς το μισθό τους.
SELECT e.empid,e.firstname FROM  employees e 
INNER JOIN departments d ON d.depid=e.depid
WHERE d.depid IN ('1','2','4') AND (e.salary BETWEEN 1000 AND 1500)
ORDER BY e.salary desc;

-- B12) Να βρεθεί ο κωδικός τμήματος στο οποίο δεν προσελήφθησαν υπάλληλοι το πρώτο εξάμηνο του 2003.

SELECT e.depid, e.hiredate FROM employees e WHERE e.hiredate   BETWEEN '2003-01-01' AND '2003-06-31';

-- εδω χρησιμοποιησα αντιθετη λογικη δηλαδη φερε μου οσα τμηματα υπαρχουν στο πρωτο εξαμηνο του 2003 (3 και 5) επομενως οσα δεν επεστρεψε το query ειναι αυτα
-- στα οποια δεν προσεληφθησαν υπαλληλοι

-- B13) Να βρεθούν οι υπάλληλοι (όνομα, επώνυμο) με κωδικό 100 ως 500.

SELECT e.firstname,e.lastname FROM employees e WHERE e.empid BETWEEN 100 AND 500;

-- B14)Να βρεθεί το πλήθος των προσλήψεων ανά έτος πρόσληψης, με αύξουσα ταξινόμηση ως προς το πλήθος προσλήψεων.

SELECT COUNT(empid) AS plithosproslipseon, YEAR(hiredate) FROM employees
GROUP BY 2
ORDER BY 1 asc;

-- B15)Να βρεθεί η τελευταία ημερομηνία πρόσληψης ανά τμήμα.

SELECT d.depname, MAX(e.hiredate) FROM employees e
INNER JOIN departments d ON d.depid=e.depid
GROUP BY 1;



-- B16)Να βρεθεί ο μεγαλύτερος μισθός ανά τμήμα υπαλλήλου.

SELECT d.depname, MAX(e.salary) FROM employees e
INNER JOIN departments d ON d.depid=e.depid
GROUP BY 1;

-- B17) Να βρεθεί ο μεγαλύτερος μισθός ανά τμήμα υπαλλήλου, για τους υπαλλήλους που προσελήφθησαν μετά την 1/1/2002.

SELECT d.depname, MAX(e.salary) FROM employees e
INNER JOIN departments d ON d.depid=e.depid
WHERE e.hiredate>='2002-01-01'
GROUP BY 1;

-- B18) Να βρεθούν τα ονόματα και το τμήμα των υπαλλήλων που προσελήφθησαν μέσα στο πρώτο εξάμηνο του 2004 και ο μισθός τους είναι μεγαλύτερος ή ίσος των 1350 €.

SELECT e.firstname,e.lastname,d.depname FROM employees e
INNER JOIN departments d ON d.depid=e.depid
WHERE (e.hiredate BETWEEN '2004-01-01' AND '2004-06-31' ) AND e.salary>=1350;


-- B19) Να βρεθεί ο τίτλος και η ημερομηνία έναρξης όλων των έργων στα οποία απασχολούνται υπάλληλοι με μισθό μεγαλύτερο του 1500.

SELECT distinct p.title,p.startdate FROM employees e
INNER JOIN workson w ON w.empid=e.empid
INNER JOIN projects p ON w.projid=p.projid
WHERE e.salary>1500;





-- B20) Να βρεθεί ο τίτλος και η ημερομηνία έναρξης όλων των έργων στα οποία απασχολούνται υπάλληλοι που, αν πάρουν αύξηση 3.5%, ο μισθός τους θα μεγαλώσει μεταξύ 40 και 100 €.


SELECT distinct p.title,p.startdate FROM employees e
INNER JOIN workson w ON w.empid=e.empid
INNER JOIN projects p ON w.projid=p.projid
WHERE (e.salary*3.5)/100 BETWEEN 40 AND 100;







-- B21) Να βρεθεί το άθροισμα των προϋπολογισμών των έργων στα οποία συμμετέχουν οι υπάλληλοι του τμήματος με κωδικό 1 και του τμήματος με κωδικό 2.


SELECT distinct sum(p.budget),e.depid FROM employees e
INNER JOIN workson w ON w.empid=e.empid
INNER JOIN projects p ON w.projid=p.projid
WHERE e.depid IN ('1','2')
GROUP BY 2;

-- εδω αθροισα ξεχωριστα τους προυπολογισμους για καθε τμημα. αν βγαλουμε το group by θα εχουμε τον συνολικο. Μπορουμε επισης να βαλουμε και rollup

--   /************************* PART A ***********************************/


-- a) Δώστε παράδειγμα SELECT που να περιλαμβάνει ταξινόμηση (order by). Γράψτε σχετική εκφώνηση για το τι κάνει το παράδειγμά.

-- Να ταξινομηθουν τα επωνυμα και τα ονοματα των υπαλληλων σε φθινουσα σειρα

SELECT e.firstname,e.lastname FROM employees e
ORDER BY 1,2 desc;

-- b) Δώστε παράδειγμα SELECT που να περιλαμβάνει GROUP BY. Γράψτε σχετική εκφώνηση για το τι κάνει το παράδειγμά. 
 
-- Να βρεθει το αθροισμα των υπαλληλων ανα τμημα
 
SELECT COUNT(empid),depid FROM employees
GROUP BY 2;
 
--  c) Δώστε παράδειγμα SELECT που να περιλαμβάνει σύνδεση (join) πινάκων. Γράψτε τι κάνει το παράδειγμά σας.


-- Να βρεθει ο αριθμος των πτυχιων και το ονομα του πτυχιου για καθε τμημα

SELECT COUNT(e.ptixio),e.ptixio,d.depname FROM employees e
INNER JOIN departments d ON  d.depid=e.depid
GROUP BY 2,3 desc;

 -- d) Δώστε παράδειγμα SELECT που να περιλαμβάνει IN. Γράψτε τι κάνει το παράδειγμά σας. 
 
 -- Να βρεθει ο συνολικος αριθμος των υπαλληλων που εργαζονται στο περιστερι και στο αιγαλεω ανστιστοιχα
 
 SELECT COUNT(e.empid),d.location FROM employees e
INNER JOIN departments d ON  d.depid=e.depid
WHERE d.location IN ('PERISTERI','EGALEO')
GROUP BY 2;
 
-- e) Δώστε παράδειγμα SELECT που να περιλαμβάνει σύνδεση (join) 3 πινάκων. Γράψτε τι κάνει το παράδειγμά σας.


-- Να βρεθει ο αριθμος των υπαλληλων που εργαζονται σε προτζεκτ με προοδο μεγαλυτερη ιση του 60%

SELECT distinct count(e.empid) FROM employees e
INNER JOIN workson w ON w.empid=e.empid
INNER JOIN projects p ON w.projid=p.projid
WHERE p.progress>=60;






























